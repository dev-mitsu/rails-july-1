#コンソールに入力することができる。

puts "名前を入力してください。"
your_name = gets.chomp #getsコマンドでターミナルに入力を要請し、chompコマンドで改行文字を消す。

puts "あなたの名前は#{your_name}です。"
